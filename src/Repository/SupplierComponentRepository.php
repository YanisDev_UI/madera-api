<?php

namespace App\Repository;

use App\Entity\SupplierComponent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SupplierComponent|null find($id, $lockMode = null, $lockVersion = null)
 * @method SupplierComponent|null findOneBy(array $criteria, array $orderBy = null)
 * @method SupplierComponent[]    findAll()
 * @method SupplierComponent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SupplierComponentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupplierComponent::class);
    }

    // /**
    //  * @return SupplierComponent[] Returns an array of SupplierComponent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SupplierComponent
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
