<?php

namespace App\Repository;

use App\Entity\InsulationType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InsulationType|null find($id, $lockMode = null, $lockVersion = null)
 * @method InsulationType|null findOneBy(array $criteria, array $orderBy = null)
 * @method InsulationType[]    findAll()
 * @method InsulationType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InsulationTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InsulationType::class);
    }

    // /**
    //  * @return InsulationType[] Returns an array of InsulationType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InsulationType
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
