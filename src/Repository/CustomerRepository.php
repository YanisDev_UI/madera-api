<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    /**
      * @return Customer[] Returns an array of Customer objects
      */
    
    public function findByProjectID($ProjectID)
    {
        return $this->createQueryBuilder('c')
            
            ->innerJoin(Project::class, 'p', 'WITH', 'p.customer = c.Id and p.id = :val')
            ->setParameter('val', $ProjectID)
            ->orderBy('c.Id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
        // return $this->getEntityManager()
        // ->createQuery("SELECT C.id FROM customer C inner JOIN project P ON P.customer_id = C.id and P.id = :val")
        // ->setParameter('val', $ProjectID)
        // ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Customer
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
