<?php

namespace App\Repository;

use App\Entity\StorageComponent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StorageComponent|null find($id, $lockMode = null, $lockVersion = null)
 * @method StorageComponent|null findOneBy(array $criteria, array $orderBy = null)
 * @method StorageComponent[]    findAll()
 * @method StorageComponent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StorageComponentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StorageComponent::class);
    }

    // /**
    //  * @return StorageComponent[] Returns an array of StorageComponent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StorageComponent
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
