<?php

namespace App\Repository;

use App\Entity\RangeModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RangeModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method RangeModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method RangeModule[]    findAll()
 * @method RangeModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RangeModuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RangeModule::class);
    }

    // /**
    //  * @return RangeModule[] Returns an array of RangeModule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RangeModule
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
