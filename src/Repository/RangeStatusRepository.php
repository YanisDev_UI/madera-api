<?php

namespace App\Repository;

use App\Entity\RangeStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RangeStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method RangeStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method RangeStatus[]    findAll()
 * @method RangeStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RangeStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RangeStatus::class);
    }

    // /**
    //  * @return RangeStatus[] Returns an array of RangeStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RangeStatus
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
