<?php

namespace App\Repository;

use App\Entity\ModuleProject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ModuleProject|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModuleProject|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModuleProject[]    findAll()
 * @method ModuleProject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModuleProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModuleProject::class);
    }

    // /**
    //  * @return ModuleProject[] Returns an array of ModuleProject objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ModuleProject
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
