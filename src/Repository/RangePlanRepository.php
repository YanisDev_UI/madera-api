<?php

namespace App\Repository;

use App\Entity\RangePlan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RangePlan|null find($id, $lockMode = null, $lockVersion = null)
 * @method RangePlan|null findOneBy(array $criteria, array $orderBy = null)
 * @method RangePlan[]    findAll()
 * @method RangePlan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RangePlanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RangePlan::class);
    }

    // /**
    //  * @return RangePlan[] Returns an array of RangePlan objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RangePlan
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
