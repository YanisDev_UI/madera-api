<?php

namespace App\Entity;

use App\Repository\RangeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=RangeRepository::class)
 * @ORM\Table(name="`range`")
 */
class Range 
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $finition;

    /**
     * @ORM\OneToMany(targetEntity=InsulationType::class, mappedBy="Ranges")
     */
    private $insulation_types;

    /**
     * @ORM\ManyToOne(targetEntity=InsulationType::class, inversedBy="Ranges")
     */
    private $insulation_type;

    /**
     * @ORM\ManyToOne(targetEntity=FrameQuality::class, inversedBy="Ranges")
     */
    private $frame_quality;

    /**
     * @ORM\ManyToOne(targetEntity=CoverageType::class, inversedBy="Ranges")
     */
    private $coverage_type;

    /**
     * @ORM\OneToMany(targetEntity=RangeStatus::class, mappedBy="unique_range")
     */
    private $RangeStatus;

    /**
     * @ORM\OneToMany(targetEntity=RangeModule::class, mappedBy="range_id")
     */
    private $rangeModules;

    /**
     * @ORM\OneToMany(targetEntity=RangePlan::class, mappedBy="range_id")
     */
    private $rangePlans;

    public function __construct()
    {
        $this->RangeStatus = new ArrayCollection();
        $this->rangeModules = new ArrayCollection();
        $this->rangePlans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFinition(): ?string
    {
        return $this->finition;
    }

    public function setFinition(?string $finition): self
    {
        $this->finition = $finition;

        return $this;
    }

    public function getInsulationType(): ?InsulationType
    {
        return $this->insulation_type;
    }

    public function setInsulationType(?InsulationType $insulation_type): self
    {
        $this->insulation_type = $insulation_type;

        return $this;
    }

    public function getFrameQuality(): ?FrameQuality
    {
        return $this->frame_quality;
    }

    public function setFrameQuality(?FrameQuality $frame_quality): self
    {
        $this->frame_quality = $frame_quality;

        return $this;
    }

    public function getCoverageType(): ?CoverageType
    {
        return $this->coverage_type;
    }

    public function setCoverageType(?CoverageType $coverage_type): self
    {
        $this->coverage_type = $coverage_type;

        return $this;
    }

    /**
     * @return Collection|RangeStatus[]
     */
    public function getRangeStatus(): Collection
    {
        return $this->RangeStatus;
    }

    public function addRangeStatus(RangeStatus $rangeStatus): self
    {
        if (!$this->RangeStatus->contains($rangeStatus)) {
            $this->RangeStatus[] = $rangeStatus;
            $rangeStatus->setRange($this);
        }

        return $this;
    }

    public function removeRangeStatus(RangeStatus $rangeStatus): self
    {
        if ($this->RangeStatus->removeElement($rangeStatus)) {
            // set the owning side to null (unless already changed)
            if ($rangeStatus->getRange() === $this) {
                $rangeStatus->setRange(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RangeModule[]
     */
    public function getRangeModules(): Collection
    {
        return $this->rangeModules;
    }

    public function addRangeModule(RangeModule $rangeModule): self
    {
        if (!$this->rangeModules->contains($rangeModule)) {
            $this->rangeModules[] = $rangeModule;
            $rangeModule->setRangeId($this);
        }

        return $this;
    }

    public function removeRangeModule(RangeModule $rangeModule): self
    {
        if ($this->rangeModules->removeElement($rangeModule)) {
            // set the owning side to null (unless already changed)
            if ($rangeModule->getRangeId() === $this) {
                $rangeModule->setRangeId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RangePlan[]
     */
    public function getRangePlans(): Collection
    {
        return $this->rangePlans;
    }

    public function addRangePlan(RangePlan $rangePlan): self
    {
        if (!$this->rangePlans->contains($rangePlan)) {
            $this->rangePlans[] = $rangePlan;
            $rangePlan->setRangeId($this);
        }

        return $this;
    }

    public function removeRangePlan(RangePlan $rangePlan): self
    {
        if ($this->rangePlans->removeElement($rangePlan)) {
            // set the owning side to null (unless already changed)
            if ($rangePlan->getRangeId() === $this) {
                $rangePlan->setRangeId(null);
            }
        }

        return $this;
    }
}
