<?php

namespace App\Entity;

use App\Repository\ComponentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ComponentRepository::class)
 * 
 */
class Component 
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Familly;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $usage_unit;



    /**
     * @ORM\OneToMany(targetEntity=ComponentModule::class, mappedBy="component_id")
     */
    private $componentModules;

    /**
     * @ORM\OneToMany(targetEntity=StorageComponent::class, mappedBy="component_id")
     */
    private $storageComponents;

    /**
     * @ORM\OneToMany(targetEntity=SupplierComponent::class, mappedBy="component_id")
     */
    private $supplierComponents;


    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $updated_date;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $created_date;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->created_date;
    }

    public function setCreatedDate(\DateTimeInterface $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updated_date;
    }

    public function setUpdatedDate(?\DateTimeInterface $updated_date): self
    {
        $this->updated_date = $updated_date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    
    public function __construct()
    {
        $this->componentModules = new ArrayCollection();
        $this->storageComponents = new ArrayCollection();
        $this->supplierComponents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFamilly(): ?string
    {
        return $this->Familly;
    }

    public function setFamilly(string $Familly): self
    {
        $this->Familly = $Familly;

        return $this;
    }

    public function getUsageUnit(): ?string
    {
        return $this->usage_unit;
    }

    public function setUsageUnit(?string $usage_unit): self
    {
        $this->usage_unit = $usage_unit;

        return $this;
    }

    /**
     * @return Collection|ComponentModule[]
     */
    public function getComponentModules(): Collection
    {
        return $this->componentModules;
    }

    public function addComponentModule(ComponentModule $componentModule): self
    {
        if (!$this->componentModules->contains($componentModule)) {
            $this->componentModules[] = $componentModule;
            $componentModule->setComponentId($this);
        }

        return $this;
    }

    public function removeComponentModule(ComponentModule $componentModule): self
    {
        if ($this->componentModules->removeElement($componentModule)) {
            // set the owning side to null (unless already changed)
            if ($componentModule->getComponentId() === $this) {
                $componentModule->setComponentId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StorageComponent[]
     */
    public function getStorageComponents(): Collection
    {
        return $this->storageComponents;
    }

    public function addStorageComponent(StorageComponent $storageComponent): self
    {
        if (!$this->storageComponents->contains($storageComponent)) {
            $this->storageComponents[] = $storageComponent;
            $storageComponent->setComponentId($this);
        }

        return $this;
    }

    public function removeStorageComponent(StorageComponent $storageComponent): self
    {
        if ($this->storageComponents->removeElement($storageComponent)) {
            // set the owning side to null (unless already changed)
            if ($storageComponent->getComponentId() === $this) {
                $storageComponent->setComponentId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SupplierComponent[]
     */
    public function getSupplierComponents(): Collection
    {
        return $this->supplierComponents;
    }

    public function addSupplierComponent(SupplierComponent $supplierComponent): self
    {
        if (!$this->supplierComponents->contains($supplierComponent)) {
            $this->supplierComponents[] = $supplierComponent;
            $supplierComponent->setComponentId($this);
        }

        return $this;
    }

    public function removeSupplierComponent(SupplierComponent $supplierComponent): self
    {
        if ($this->supplierComponents->removeElement($supplierComponent)) {
            // set the owning side to null (unless already changed)
            if ($supplierComponent->getComponentId() === $this) {
                $supplierComponent->setComponentId(null);
            }
        }

        return $this;
    }
}
