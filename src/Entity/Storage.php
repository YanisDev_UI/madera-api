<?php

namespace App\Entity;

use App\Repository\StorageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=StorageRepository::class)
 */
class Storage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $City;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postal_code;

    /**
     * @ORM\OneToMany(targetEntity=StorageComponent::class, mappedBy="storage_id")
     */
    private $storageComponents;

    public function __construct()
    {
        $this->storageComponents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?string
    {
        return $this->City;
    }

    public function setCity(string $City): self
    {
        $this->City = $City;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->Street;
    }

    public function setStreet(?string $Street): self
    {
        $this->Street = $Street;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postal_code;
    }

    public function setPostalCode(?string $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    /**
     * @return Collection|StorageComponent[]
     */
    public function getStorageComponents(): Collection
    {
        return $this->storageComponents;
    }

    public function addStorageComponent(StorageComponent $storageComponent): self
    {
        if (!$this->storageComponents->contains($storageComponent)) {
            $this->storageComponents[] = $storageComponent;
            $storageComponent->setStorageId($this);
        }

        return $this;
    }

    public function removeStorageComponent(StorageComponent $storageComponent): self
    {
        if ($this->storageComponents->removeElement($storageComponent)) {
            // set the owning side to null (unless already changed)
            if ($storageComponent->getStorageId() === $this) {
                $storageComponent->setStorageId(null);
            }
        }

        return $this;
    }
}
