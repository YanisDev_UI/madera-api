<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project 
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="related_projects")
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity=Quotation::class, mappedBy="project")
     */
    private $quotations;

    /**
     * @ORM\OneToMany(targetEntity=ModuleProject::class, mappedBy="project_id")
     */
    private $moduleProjects;

    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $updated_date;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $created_date;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->created_date;
    }

    public function setCreatedDate(\DateTimeInterface $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updated_date;
    }

    public function setUpdatedDate(?\DateTimeInterface $updated_date): self
    {
        $this->updated_date = $updated_date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    

    public function __construct()
    {
        $this->quotations = new ArrayCollection();
        $this->moduleProjects = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|Quotation[]
     */
    public function getQuotations(): Collection
    {
        return $this->quotations;
    }

    public function addQuotation(Quotation $quotation): self
    {
        if (!$this->quotations->contains($quotation)) {
            $this->quotations[] = $quotation;
            $quotation->setProject($this);
        }

        return $this;
    }

    public function removeQuotation(Quotation $quotation): self
    {
        if ($this->quotations->removeElement($quotation)) {
            // set the owning side to null (unless already changed)
            if ($quotation->getProject() === $this) {
                $quotation->setProject(null);
            }
        }

        return $this;
    }



    /**
     * @return Collection|ModuleProject[]
     */
    public function getModuleProjects(): Collection
    {
        return $this->moduleProjects;
    }

    public function addModuleProject(ModuleProject $moduleProject): self
    {
        if (!$this->moduleProjects->contains($moduleProject)) {
            $this->moduleProjects[] = $moduleProject;
            $moduleProject->setProjectId($this);
        }

        return $this;
    }

    public function removeModuleProject(ModuleProject $moduleProject): self
    {
        if ($this->moduleProjects->removeElement($moduleProject)) {
            // set the owning side to null (unless already changed)
            if ($moduleProject->getProjectId() === $this) {
                $moduleProject->setProjectId(null);
            }
        }

        return $this;
    }
}
