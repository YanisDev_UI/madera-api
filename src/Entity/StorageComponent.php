<?php

namespace App\Entity;

use App\Repository\StorageComponentRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=StorageComponentRepository::class)
 */
class StorageComponent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Storage::class, inversedBy="storageComponents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $storage_id;

    /**
     * @ORM\ManyToOne(targetEntity=Component::class, inversedBy="storageComponents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $component_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStorageId(): ?Storage
    {
        return $this->storage_id;
    }

    public function setStorageId(?Storage $storage_id): self
    {
        $this->storage_id = $storage_id;

        return $this;
    }

    public function getComponentId(): ?Component
    {
        return $this->component_id;
    }

    public function setComponentId(?Component $component_id): self
    {
        $this->component_id = $component_id;

        return $this;
    }
}
