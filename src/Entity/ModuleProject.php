<?php

namespace App\Entity;

use App\Repository\ModuleProjectRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ModuleProjectRepository::class)
 */
class ModuleProject
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Module::class, inversedBy="moduleProjects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $module_id;

    /**
     * @ORM\ManyToOne(targetEntity=project::class, inversedBy="moduleProjects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModuleId(): ?Module
    {
        return $this->module_id;
    }

    public function setModuleId(?Module $module_id): self
    {
        $this->module_id = $module_id;

        return $this;
    }

    public function getProjectId(): ?Project
    {
        return $this->project_id;
    }

    public function setProjectId(?Project $project_id): self
    {
        $this->project_id = $project_id;

        return $this;
    }
}
