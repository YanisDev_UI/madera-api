<?php

namespace App\Entity;

use App\Repository\ComponentModuleRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ComponentModuleRepository::class)
 */
class ComponentModule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Component::class, inversedBy="componentModules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $component_id;

    /**
     * @ORM\ManyToOne(targetEntity=Module::class, inversedBy="componentModules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $module_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComponentId(): ?Component
    {
        return $this->component_id;
    }

    public function setComponentId(?Component $component_id): self
    {
        $this->component_id = $component_id;

        return $this;
    }

    public function getModuleId(): ?Module
    {
        return $this->module_id;
    }

    public function setModuleId(?Module $module_id): self
    {
        $this->module_id = $module_id;

        return $this;
    }
}
