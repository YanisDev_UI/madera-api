<?php

namespace App\Entity;

use App\Repository\QuotationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(
  *     itemOperations={
 *         "view_myself"={
 *             "route_name"="quotationstatusbyquotationid",
 *             "swagger_context"={
 *                  "parameters"={} 
 *              }
 *              
 *         },
 *  "put", "delete","get"
 *     }
 * )
 * @ORM\Entity(repositoryClass=QuotationRepository::class)
 */
class Quotation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quote_unit;

    
 

    /**
     * @ORM\Column(type="date")
     */
    private $generated_date;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ht_price;

    /**
     * @ORM\Column(type="float")
     */
    private $ttc_price;

     /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="quotations")
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity=QuotationStatus::class, mappedBy="Quotation")
     */
    private $Status;

    /**
     * @ORM\OneToMany(targetEntity=Plan::class, mappedBy="quotation")
     */
    private $plan;

    public function __construct()
    {
        $this->Status = new ArrayCollection();
        $this->plan = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuoteUnit(): ?string
    {
        return $this->quote_unit;
    }

    public function setQuoteUnit(?string $quote_unit): self
    {
        $this->quote_unit = $quote_unit;

        return $this;
    }

    public function getGeneratedDate(): ?\DateTimeInterface
    {
        return $this->generated_date;
    }

    public function setGeneratedDate(\DateTimeInterface $generated_date): self
    {
        $this->generated_date = $generated_date;

        return $this;
    }

    public function getHtPrice(): ?float
    {
        return $this->ht_price;
    }

    public function setHtPrice(?float $ht_price): self
    {
        $this->ht_price = $ht_price;

        return $this;
    }

    public function getTtcPrice(): ?float
    {
        return $this->ttc_price;
    }

    public function setTtcPrice(float $ttc_price): self
    {
        $this->ttc_price = $ttc_price;

        return $this;
    }

    /**
     * @return Collection|quotationstatus[]
     */
    public function getStatus(): Collection
    {
        return $this->Status;
    }

    public function addStatus(quotationstatus $status): self
    {
        if (!$this->Status->contains($status)) {
            $this->Status[] = $status;
            $status->setQuotation($this);
        }

        return $this;
    }

    public function removeStatus(quotationstatus $status): self
    {
        if ($this->Status->removeElement($status)) {
            // set the owning side to null (unless already changed)
            if ($status->getQuotation() === $this) {
                $status->setQuotation(null);
            }
        }

        return $this;
    }
    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Collection|Plan[]
     */
    public function getPlan(): Collection
    {
        return $this->plan;
    }

    public function addPlan(Plan $plan): self
    {
        if (!$this->plan->contains($plan)) {
            $this->plan[] = $plan;
            $plan->setQuotation($this);
        }

        return $this;
    }

    public function removePlan(Plan $plan): self
    {
        if ($this->plan->removeElement($plan)) {
            // set the owning side to null (unless already changed)
            if ($plan->getQuotation() === $this) {
                $plan->setQuotation(null);
            }
        }

        return $this;
    }
}
