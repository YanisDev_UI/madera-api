<?php

namespace App\Entity;

use App\Repository\CoverageTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CoverageTypeRepository::class)
 */
class CoverageType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $designation;

    /**
     * @ORM\OneToMany(targetEntity=Range::class, mappedBy="coverage_type")
     */
    private $Ranges;


    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $updated_date;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $created_date;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->created_date;
    }

    public function setCreatedDate(\DateTimeInterface $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updated_date;
    }

    public function setUpdatedDate(?\DateTimeInterface $updated_date): self
    {
        $this->updated_date = $updated_date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function __construct()
    {
        $this->Ranges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(?string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * @return Collection|Range[]
     */
    public function getRanges(): Collection
    {
        return $this->Ranges;
    }

    public function addRange(Range $range): self
    {
        if (!$this->Ranges->contains($range)) {
            $this->Ranges[] = $range;
            $range->setCoverageType($this);
        }

        return $this;
    }

    public function removeRange(Range $range): self
    {
        if ($this->Ranges->removeElement($range)) {
            // set the owning side to null (unless already changed)
            if ($range->getCoverageType() === $this) {
                $range->setCoverageType(null);
            }
        }

        return $this;
    }
}
