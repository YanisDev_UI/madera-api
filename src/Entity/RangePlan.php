<?php

namespace App\Entity;

use App\Repository\RangePlanRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=RangePlanRepository::class)
 */
class RangePlan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Range::class, inversedBy="rangePlans")
     * @ORM\JoinColumn(nullable=false)
     */
    private $range_id;

    /**
     * @ORM\ManyToOne(targetEntity=Plan::class, inversedBy="rangePlans")
     * @ORM\JoinColumn(nullable=false)
     */
    private $plan_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRangeId(): ?Range
    {
        return $this->range_id;
    }

    public function setRangeId(?Range $range_id): self
    {
        $this->range_id = $range_id;

        return $this;
    }

    public function getPlanId(): ?Plan
    {
        return $this->plan_id;
    }

    public function setPlanId(?Plan $plan_id): self
    {
        $this->plan_id = $plan_id;

        return $this;
    }
}
