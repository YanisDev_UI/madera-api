<?php

namespace App\Entity;

use App\Repository\RangeModuleRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=RangeModuleRepository::class)
 */
class RangeModule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=range::class, inversedBy="rangeModules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $range_id;

    /**
     * @ORM\ManyToOne(targetEntity=Module::class, inversedBy="rangeModules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $module_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRangeId(): ?range
    {
        return $this->range_id;
    }

    public function setRangeId(?range $range_id): self
    {
        $this->range_id = $range_id;

        return $this;
    }

    public function getModuleId(): ?Module
    {
        return $this->module_id;
    }

    public function setModuleId(?Module $module_id): self
    {
        $this->module_id = $module_id;

        return $this;
    }
}
