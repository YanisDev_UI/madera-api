<?php

namespace App\Entity;

use App\Repository\PlanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(
*     itemOperations={
 *         "view_myself"={
 *             "route_name"="plansbyquotationid",
 *             "swagger_context"={
 *                  "parameters"={}
 *              }
 *              
 *         },
 *  "put", "delete","get"
 *     }
 * )
 * @ORM\Entity(repositoryClass=PlanRepository::class)
 */
class Plan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;





    /**
     * @ORM\OneToMany(targetEntity=RangePlan::class, mappedBy="plan_id")
     */
    private $rangePlans;


    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $updated_date;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $created_date;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Quotation::class, inversedBy="plan")
     */
    private $quotation;

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->created_date;
    }

    public function setCreatedDate(\DateTimeInterface $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updated_date;
    }

    public function setUpdatedDate(?\DateTimeInterface $updated_date): self
    {
        $this->updated_date = $updated_date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    public function __construct()
    {
        $this->rangePlans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }




    /**
     * @return Collection|RangePlan[]
     */
    public function getRangePlans(): Collection
    {
        return $this->rangePlans;
    }

    public function addRangePlan(RangePlan $rangePlan): self
    {
        if (!$this->rangePlans->contains($rangePlan)) {
            $this->rangePlans[] = $rangePlan;
            $rangePlan->setPlanId($this);
        }

        return $this;
    }

    public function removeRangePlan(RangePlan $rangePlan): self
    {
        if ($this->rangePlans->removeElement($rangePlan)) {
            // set the owning side to null (unless already changed)
            if ($rangePlan->getPlanId() === $this) {
                $rangePlan->setPlanId(null);
            }
        }

        return $this;
    }

    public function getQuotation(): ?Quotation
    {
        return $this->quotation;
    }

    public function setQuotation(?Quotation $quotation): self
    {
        $this->quotation = $quotation;

        return $this;
    }
}
