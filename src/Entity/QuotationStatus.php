<?php

namespace App\Entity;

use App\Repository\QuotationStatusRepository;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(
   *     itemOperations={
 *         "view_myself"={
 *             "route_name"="quotationsbyprojectid",
 *             "swagger_context"={
 *                  "parameters"={} 
 *              }
 *              
 *         },
 *  "put", "delete","get"
 *     }
 * )
 * @ORM\Entity(repositoryClass=QuotationStatusRepository::class)
 */
class QuotationStatus 
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity=Quotation::class, inversedBy="Status")
     */
    private $Quotation;

    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $updated_date;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $created_date;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->created_date;
    }

    public function setCreatedDate(\DateTimeInterface $created_date): self
    {
        $this->created_date = $created_date;

        return $this;
    }

    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updated_date;
    }

    public function setUpdatedDate(?\DateTimeInterface $updated_date): self
    {
        $this->updated_date = $updated_date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuotation(): ?QUotation
    {
        return $this->Quotation;
    }

    public function setQuotation(?QUotation $Quotation): self
    {
        $this->Quotation = $Quotation;

        return $this;
    }


}
