<?php

namespace App\Entity;

use App\Repository\ModuleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ModuleRepository::class)
 */
class Module
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nature;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $features;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $usage_unit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $main_section;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cctp;

    /**
     * @ORM\ManyToOne(targetEntity=ModuleStatus::class, inversedBy="modules")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity=ComponentModule::class, mappedBy="module_id")
     */
    private $componentModules;

    /**
     * @ORM\OneToMany(targetEntity=ModuleProject::class, mappedBy="module_id")
     */
    private $moduleProjects;

    /**
     * @ORM\OneToMany(targetEntity=RangeModule::class, mappedBy="module_id")
     */
    private $rangeModules;

    public function __construct()
    {
        $this->componentModules = new ArrayCollection();
        $this->moduleProjects = new ArrayCollection();
        $this->rangeModules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNature(): ?string
    {
        return $this->nature;
    }

    public function setNature(?string $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    public function getFeatures(): ?string
    {
        return $this->features;
    }

    public function setFeatures(?string $features): self
    {
        $this->features = $features;

        return $this;
    }

    public function getUsageUnit(): ?string
    {
        return $this->usage_unit;
    }

    public function setUsageUnit(?string $usage_unit): self
    {
        $this->usage_unit = $usage_unit;

        return $this;
    }

    public function getMainSection(): ?string
    {
        return $this->main_section;
    }

    public function setMainSection(?string $main_section): self
    {
        $this->main_section = $main_section;

        return $this;
    }

    public function getCctp(): ?string
    {
        return $this->cctp;
    }

    public function setCctp(?string $cctp): self
    {
        $this->cctp = $cctp;

        return $this;
    }

    public function getStatus(): ?ModuleStatus
    {
        return $this->status;
    }

    public function setStatus(?ModuleStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ComponentModule[]
     */
    public function getComponentModules(): Collection
    {
        return $this->componentModules;
    }

    public function addComponentModule(ComponentModule $componentModule): self
    {
        if (!$this->componentModules->contains($componentModule)) {
            $this->componentModules[] = $componentModule;
            $componentModule->setModuleId($this);
        }

        return $this;
    }

    public function removeComponentModule(ComponentModule $componentModule): self
    {
        if ($this->componentModules->removeElement($componentModule)) {
            // set the owning side to null (unless already changed)
            if ($componentModule->getModuleId() === $this) {
                $componentModule->setModuleId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ModuleProject[]
     */
    public function getModuleProjects(): Collection
    {
        return $this->moduleProjects;
    }

    public function addModuleProject(ModuleProject $moduleProject): self
    {
        if (!$this->moduleProjects->contains($moduleProject)) {
            $this->moduleProjects[] = $moduleProject;
            $moduleProject->setModuleId($this);
        }

        return $this;
    }

    public function removeModuleProject(ModuleProject $moduleProject): self
    {
        if ($this->moduleProjects->removeElement($moduleProject)) {
            // set the owning side to null (unless already changed)
            if ($moduleProject->getModuleId() === $this) {
                $moduleProject->setModuleId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RangeModule[]
     */
    public function getRangeModules(): Collection
    {
        return $this->rangeModules;
    }

    public function addRangeModule(RangeModule $rangeModule): self
    {
        if (!$this->rangeModules->contains($rangeModule)) {
            $this->rangeModules[] = $rangeModule;
            $rangeModule->setModuleId($this);
        }

        return $this;
    }

    public function removeRangeModule(RangeModule $rangeModule): self
    {
        if ($this->rangeModules->removeElement($rangeModule)) {
            // set the owning side to null (unless already changed)
            if ($rangeModule->getModuleId() === $this) {
                $rangeModule->setModuleId(null);
            }
        }

        return $this;
    }
}
