<?php

namespace App\Entity;

use App\Repository\SupplierComponentRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=SupplierComponentRepository::class)
 */
class SupplierComponent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Supplier::class, inversedBy="supplierComponents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $supplier_id;

    /**
     * @ORM\ManyToOne(targetEntity=Component::class, inversedBy="supplierComponents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $component_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSupplierId(): ?Supplier
    {
        return $this->supplier_id;
    }

    public function setSupplierId(?Supplier $supplier_id): self
    {
        $this->supplier_id = $supplier_id;

        return $this;
    }

    public function getComponentId(): ?Component
    {
        return $this->component_id;
    }

    public function setComponentId(?Component $component_id): self
    {
        $this->component_id = $component_id;

        return $this;
    }
}
