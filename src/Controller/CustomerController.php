<?php

namespace App\Controller;

use App\Entity\Project;
use App\Repository\CustomerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CustomerController extends AbstractController
{
    /**
     * @Route("/customers/{id}/project", name="customersbyprojectid", methods={"GET"}, defaults={"_api_item_operation_name"="CustomersByProjectID"})
     */
    public function index(Request $request, int $id, Project $project, CustomerRepository $customerRepository)  {

        //dd($customerRepository->findByProjectID($id));
        return $this->json($customerRepository->findByProjectID($id));
    }
}
