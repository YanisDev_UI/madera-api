<?php

namespace App\Controller;

use App\Repository\QuotationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuotationController extends AbstractController
{
    /**
     * @Route("/quotations/{id}/project", name="quotationsbyprojectid", methods={"GET"}, defaults={"_api_item_operation_name"="QuotationsByProjectID"})
     */
    public function index( int $id,  QuotationRepository $quotationRepository)  {

        //dd($customerRepository->findByProjectID($id));
        return $this->json($quotationRepository->findBy(["project" => $id]));
    }
}
