<?php

namespace App\Controller;

use App\Repository\QuotationStatusRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuotationStatusController extends AbstractController
{
    /**
     * @Route("/quotationstatus/{id}/quotation", name="quotationstatusbyquotationid", methods={"GET"}, defaults={"_api_item_operation_name"="QuotationStatusByQuotationId"})
     */
    public function index( int $id,  QuotationStatusRepository $quotationStatusRepository)  {

        //dd($customerRepository->findByProjectID($id));
        return $this->json($quotationStatusRepository->findBy(["Quotation" => $id]));
    }
}
