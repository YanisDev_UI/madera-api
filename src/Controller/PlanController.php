<?php

namespace App\Controller;

use App\Entity\Plan;
use App\Repository\PlanRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PlanController extends AbstractController
{
    /**
     * @Route("/plans/{id}/quotation", name="plansbyquotationid", methods={"GET"}, defaults={"_api_item_operation_name"="PlansByQuotationId"})
     */
    public function index( int $id, Plan $plan, PlanRepository $planRepository)  {

        //dd($customerRepository->findByProjectID($id));
        return $this->json($planRepository->findBy(["quotation" => $id]));
    }
}
