<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324111704 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE component DROP FOREIGN KEY FK_49FEA15733B92F39');
        $this->addSql('ALTER TABLE coverage_type DROP FOREIGN KEY FK_437233433B92F39');
        $this->addSql('ALTER TABLE frame_quality DROP FOREIGN KEY FK_A77C6BBD33B92F39');
        $this->addSql('ALTER TABLE insulation_type DROP FOREIGN KEY FK_41D7AD9D33B92F39');
        $this->addSql('ALTER TABLE module_status DROP FOREIGN KEY FK_D103253333B92F39');
        $this->addSql('ALTER TABLE plan DROP FOREIGN KEY FK_DD5A5B7D33B92F39');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE33B92F39');
        $this->addSql('ALTER TABLE quotation_status DROP FOREIGN KEY FK_8170AF8633B92F39');
        $this->addSql('ALTER TABLE range_status DROP FOREIGN KEY FK_5286BB6C33B92F39');
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E09217BBB47');
        $this->addSql('ALTER TABLE supplier DROP FOREIGN KEY FK_9B2A6C7E217BBB47');
        $this->addSql('DROP TABLE label');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP INDEX UNIQ_49FEA15733B92F39 ON component');
        $this->addSql('ALTER TABLE component ADD updated_date DATE DEFAULT NULL, ADD created_date DATE DEFAULT NULL, ADD description LONGTEXT NOT NULL, DROP label_id');
        $this->addSql('ALTER TABLE component_module DROP FOREIGN KEY FK_D03D2D52AFC2B591');
        $this->addSql('ALTER TABLE component_module DROP FOREIGN KEY FK_D03D2D52E2ABAFFF');
        $this->addSql('DROP INDEX IDX_D03D2D52AFC2B591 ON component_module');
        $this->addSql('DROP INDEX IDX_D03D2D52E2ABAFFF ON component_module');
        $this->addSql('ALTER TABLE component_module ADD id INT AUTO_INCREMENT NOT NULL, ADD component_id_id INT NOT NULL, ADD module_id_id INT NOT NULL, DROP component_id, DROP module_id, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE component_module ADD CONSTRAINT FK_D03D2D52B0422B79 FOREIGN KEY (component_id_id) REFERENCES component (id)');
        $this->addSql('ALTER TABLE component_module ADD CONSTRAINT FK_D03D2D527648EE39 FOREIGN KEY (module_id_id) REFERENCES module (id)');
        $this->addSql('CREATE INDEX IDX_D03D2D52B0422B79 ON component_module (component_id_id)');
        $this->addSql('CREATE INDEX IDX_D03D2D527648EE39 ON component_module (module_id_id)');
        $this->addSql('DROP INDEX UNIQ_437233433B92F39 ON coverage_type');
        $this->addSql('ALTER TABLE coverage_type ADD updated_date DATE DEFAULT NULL, ADD created_date DATE DEFAULT NULL, ADD description LONGTEXT NOT NULL, DROP label_id');
        $this->addSql('DROP INDEX IDX_81398E09217BBB47 ON customer');
        $this->addSql('ALTER TABLE customer ADD first_name VARCHAR(255) NOT NULL, ADD last_name VARCHAR(255) NOT NULL, ADD street VARCHAR(255) NOT NULL, ADD city VARCHAR(255) DEFAULT NULL, ADD postal_code INT DEFAULT NULL, ADD country VARCHAR(255) DEFAULT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD phone_number VARCHAR(255) DEFAULT NULL, ADD company VARCHAR(255) DEFAULT NULL, ADD libelle LONGTEXT DEFAULT NULL, DROP person_id');
        $this->addSql('DROP INDEX UNIQ_A77C6BBD33B92F39 ON frame_quality');
        $this->addSql('ALTER TABLE frame_quality ADD updated_date DATE DEFAULT NULL, ADD created_date DATE DEFAULT NULL, ADD description LONGTEXT NOT NULL, DROP label_id');
        $this->addSql('DROP INDEX UNIQ_41D7AD9D33B92F39 ON insulation_type');
        $this->addSql('ALTER TABLE insulation_type ADD updated_date DATE DEFAULT NULL, ADD created_date DATE DEFAULT NULL, ADD description LONGTEXT NOT NULL, DROP label_id');
        $this->addSql('ALTER TABLE module_project DROP FOREIGN KEY FK_84C8EEF7166D1F9C');
        $this->addSql('ALTER TABLE module_project DROP FOREIGN KEY FK_84C8EEF7AFC2B591');
        $this->addSql('DROP INDEX IDX_84C8EEF7166D1F9C ON module_project');
        $this->addSql('DROP INDEX IDX_84C8EEF7AFC2B591 ON module_project');
        $this->addSql('ALTER TABLE module_project ADD id INT AUTO_INCREMENT NOT NULL, ADD module_id_id INT NOT NULL, ADD project_id_id INT NOT NULL, DROP module_id, DROP project_id, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE module_project ADD CONSTRAINT FK_84C8EEF77648EE39 FOREIGN KEY (module_id_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE module_project ADD CONSTRAINT FK_84C8EEF76C1197C9 FOREIGN KEY (project_id_id) REFERENCES project (id)');
        $this->addSql('CREATE INDEX IDX_84C8EEF77648EE39 ON module_project (module_id_id)');
        $this->addSql('CREATE INDEX IDX_84C8EEF76C1197C9 ON module_project (project_id_id)');
        $this->addSql('DROP INDEX UNIQ_D103253333B92F39 ON module_status');
        $this->addSql('ALTER TABLE module_status ADD updated_date DATE DEFAULT NULL, ADD created_date DATE DEFAULT NULL, ADD description LONGTEXT NOT NULL, DROP label_id');
        $this->addSql('DROP INDEX UNIQ_DD5A5B7D33B92F39 ON plan');
        $this->addSql('ALTER TABLE plan ADD updated_date DATE DEFAULT NULL, ADD created_date DATE DEFAULT NULL, ADD description LONGTEXT NOT NULL, DROP label_id');
        $this->addSql('DROP INDEX UNIQ_2FB3D0EE33B92F39 ON project');
        $this->addSql('ALTER TABLE project ADD updated_date DATE DEFAULT NULL, ADD created_date DATE DEFAULT NULL, ADD description LONGTEXT NOT NULL, DROP label_id');
        $this->addSql('DROP INDEX UNIQ_8170AF8633B92F39 ON quotation_status');
        $this->addSql('ALTER TABLE quotation_status ADD updated_date DATE DEFAULT NULL, ADD created_date DATE DEFAULT NULL, ADD description LONGTEXT NOT NULL, DROP label_id');
        $this->addSql('ALTER TABLE range_module DROP FOREIGN KEY FK_25A2F8582A82D0B1');
        $this->addSql('ALTER TABLE range_module DROP FOREIGN KEY FK_25A2F858AFC2B591');
        $this->addSql('DROP INDEX IDX_25A2F8582A82D0B1 ON range_module');
        $this->addSql('DROP INDEX IDX_25A2F858AFC2B591 ON range_module');
        $this->addSql('ALTER TABLE range_module ADD id INT AUTO_INCREMENT NOT NULL, ADD range_id_id INT NOT NULL, ADD module_id_id INT NOT NULL, DROP range_id, DROP module_id, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE range_module ADD CONSTRAINT FK_25A2F8582053762E FOREIGN KEY (range_id_id) REFERENCES `range` (id)');
        $this->addSql('ALTER TABLE range_module ADD CONSTRAINT FK_25A2F8587648EE39 FOREIGN KEY (module_id_id) REFERENCES module (id)');
        $this->addSql('CREATE INDEX IDX_25A2F8582053762E ON range_module (range_id_id)');
        $this->addSql('CREATE INDEX IDX_25A2F8587648EE39 ON range_module (module_id_id)');
        $this->addSql('ALTER TABLE range_plan DROP FOREIGN KEY FK_CF3A2A442A82D0B1');
        $this->addSql('ALTER TABLE range_plan DROP FOREIGN KEY FK_CF3A2A44E899029B');
        $this->addSql('DROP INDEX IDX_CF3A2A442A82D0B1 ON range_plan');
        $this->addSql('DROP INDEX IDX_CF3A2A44E899029B ON range_plan');
        $this->addSql('ALTER TABLE range_plan ADD id INT AUTO_INCREMENT NOT NULL, ADD range_id_id INT NOT NULL, ADD plan_id_id INT NOT NULL, DROP range_id, DROP plan_id, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE range_plan ADD CONSTRAINT FK_CF3A2A442053762E FOREIGN KEY (range_id_id) REFERENCES `range` (id)');
        $this->addSql('ALTER TABLE range_plan ADD CONSTRAINT FK_CF3A2A442CE2DBAB FOREIGN KEY (plan_id_id) REFERENCES plan (id)');
        $this->addSql('CREATE INDEX IDX_CF3A2A442053762E ON range_plan (range_id_id)');
        $this->addSql('CREATE INDEX IDX_CF3A2A442CE2DBAB ON range_plan (plan_id_id)');
        $this->addSql('DROP INDEX UNIQ_5286BB6C33B92F39 ON range_status');
        $this->addSql('ALTER TABLE range_status ADD updated_date DATE DEFAULT NULL, ADD created_date DATE DEFAULT NULL, ADD description LONGTEXT NOT NULL, DROP label_id');
        $this->addSql('ALTER TABLE storage_component DROP FOREIGN KEY FK_A1E5BE585CC5DB90');
        $this->addSql('ALTER TABLE storage_component DROP FOREIGN KEY FK_A1E5BE58E2ABAFFF');
        $this->addSql('DROP INDEX IDX_A1E5BE585CC5DB90 ON storage_component');
        $this->addSql('DROP INDEX IDX_A1E5BE58E2ABAFFF ON storage_component');
        $this->addSql('ALTER TABLE storage_component ADD id INT AUTO_INCREMENT NOT NULL, ADD storage_id_id INT NOT NULL, ADD component_id_id INT NOT NULL, DROP storage_id, DROP component_id, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE storage_component ADD CONSTRAINT FK_A1E5BE5812905777 FOREIGN KEY (storage_id_id) REFERENCES storage (id)');
        $this->addSql('ALTER TABLE storage_component ADD CONSTRAINT FK_A1E5BE58B0422B79 FOREIGN KEY (component_id_id) REFERENCES component (id)');
        $this->addSql('CREATE INDEX IDX_A1E5BE5812905777 ON storage_component (storage_id_id)');
        $this->addSql('CREATE INDEX IDX_A1E5BE58B0422B79 ON storage_component (component_id_id)');
        $this->addSql('DROP INDEX IDX_9B2A6C7E217BBB47 ON supplier');
        $this->addSql('ALTER TABLE supplier ADD first_name VARCHAR(255) NOT NULL, ADD last_name VARCHAR(255) NOT NULL, ADD street VARCHAR(255) NOT NULL, ADD city VARCHAR(255) DEFAULT NULL, ADD postal_code INT DEFAULT NULL, ADD country VARCHAR(255) DEFAULT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD phone_number VARCHAR(255) DEFAULT NULL, ADD company VARCHAR(255) DEFAULT NULL, ADD libelle LONGTEXT DEFAULT NULL, DROP person_id');
        $this->addSql('ALTER TABLE supplier_component DROP FOREIGN KEY FK_D3CC9B892ADD6D8C');
        $this->addSql('ALTER TABLE supplier_component DROP FOREIGN KEY FK_D3CC9B89E2ABAFFF');
        $this->addSql('DROP INDEX IDX_D3CC9B892ADD6D8C ON supplier_component');
        $this->addSql('DROP INDEX IDX_D3CC9B89E2ABAFFF ON supplier_component');
        $this->addSql('ALTER TABLE supplier_component ADD id INT AUTO_INCREMENT NOT NULL, ADD supplier_id_id INT NOT NULL, ADD component_id_id INT NOT NULL, DROP supplier_id, DROP component_id, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE supplier_component ADD CONSTRAINT FK_D3CC9B89A65F9C7D FOREIGN KEY (supplier_id_id) REFERENCES supplier (id)');
        $this->addSql('ALTER TABLE supplier_component ADD CONSTRAINT FK_D3CC9B89B0422B79 FOREIGN KEY (component_id_id) REFERENCES component (id)');
        $this->addSql('CREATE INDEX IDX_D3CC9B89A65F9C7D ON supplier_component (supplier_id_id)');
        $this->addSql('CREATE INDEX IDX_D3CC9B89B0422B79 ON supplier_component (component_id_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE label (id INT AUTO_INCREMENT NOT NULL, updated_date DATE DEFAULT NULL, created_date DATE DEFAULT NULL, description LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, last_name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, street VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, postal_code INT DEFAULT NULL, country VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, phone_number VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, company VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, libelle LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE component ADD label_id INT NOT NULL, DROP updated_date, DROP created_date, DROP description');
        $this->addSql('ALTER TABLE component ADD CONSTRAINT FK_49FEA15733B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_49FEA15733B92F39 ON component (label_id)');
        $this->addSql('ALTER TABLE component_module MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE component_module DROP FOREIGN KEY FK_D03D2D52B0422B79');
        $this->addSql('ALTER TABLE component_module DROP FOREIGN KEY FK_D03D2D527648EE39');
        $this->addSql('DROP INDEX IDX_D03D2D52B0422B79 ON component_module');
        $this->addSql('DROP INDEX IDX_D03D2D527648EE39 ON component_module');
        $this->addSql('ALTER TABLE component_module DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE component_module ADD component_id INT NOT NULL, ADD module_id INT NOT NULL, DROP id, DROP component_id_id, DROP module_id_id');
        $this->addSql('ALTER TABLE component_module ADD CONSTRAINT FK_D03D2D52AFC2B591 FOREIGN KEY (module_id) REFERENCES module (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE component_module ADD CONSTRAINT FK_D03D2D52E2ABAFFF FOREIGN KEY (component_id) REFERENCES component (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_D03D2D52AFC2B591 ON component_module (module_id)');
        $this->addSql('CREATE INDEX IDX_D03D2D52E2ABAFFF ON component_module (component_id)');
        $this->addSql('ALTER TABLE component_module ADD PRIMARY KEY (component_id, module_id)');
        $this->addSql('ALTER TABLE coverage_type ADD label_id INT NOT NULL, DROP updated_date, DROP created_date, DROP description');
        $this->addSql('ALTER TABLE coverage_type ADD CONSTRAINT FK_437233433B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_437233433B92F39 ON coverage_type (label_id)');
        $this->addSql('ALTER TABLE customer ADD person_id INT NOT NULL, DROP first_name, DROP last_name, DROP street, DROP city, DROP postal_code, DROP country, DROP email, DROP phone_number, DROP company, DROP libelle');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_81398E09217BBB47 ON customer (person_id)');
        $this->addSql('ALTER TABLE frame_quality ADD label_id INT NOT NULL, DROP updated_date, DROP created_date, DROP description');
        $this->addSql('ALTER TABLE frame_quality ADD CONSTRAINT FK_A77C6BBD33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A77C6BBD33B92F39 ON frame_quality (label_id)');
        $this->addSql('ALTER TABLE insulation_type ADD label_id INT NOT NULL, DROP updated_date, DROP created_date, DROP description');
        $this->addSql('ALTER TABLE insulation_type ADD CONSTRAINT FK_41D7AD9D33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_41D7AD9D33B92F39 ON insulation_type (label_id)');
        $this->addSql('ALTER TABLE module_project MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE module_project DROP FOREIGN KEY FK_84C8EEF77648EE39');
        $this->addSql('ALTER TABLE module_project DROP FOREIGN KEY FK_84C8EEF76C1197C9');
        $this->addSql('DROP INDEX IDX_84C8EEF77648EE39 ON module_project');
        $this->addSql('DROP INDEX IDX_84C8EEF76C1197C9 ON module_project');
        $this->addSql('ALTER TABLE module_project DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE module_project ADD module_id INT NOT NULL, ADD project_id INT NOT NULL, DROP id, DROP module_id_id, DROP project_id_id');
        $this->addSql('ALTER TABLE module_project ADD CONSTRAINT FK_84C8EEF7166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE module_project ADD CONSTRAINT FK_84C8EEF7AFC2B591 FOREIGN KEY (module_id) REFERENCES module (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_84C8EEF7166D1F9C ON module_project (project_id)');
        $this->addSql('CREATE INDEX IDX_84C8EEF7AFC2B591 ON module_project (module_id)');
        $this->addSql('ALTER TABLE module_project ADD PRIMARY KEY (module_id, project_id)');
        $this->addSql('ALTER TABLE module_status ADD label_id INT NOT NULL, DROP updated_date, DROP created_date, DROP description');
        $this->addSql('ALTER TABLE module_status ADD CONSTRAINT FK_D103253333B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D103253333B92F39 ON module_status (label_id)');
        $this->addSql('ALTER TABLE plan ADD label_id INT NOT NULL, DROP updated_date, DROP created_date, DROP description');
        $this->addSql('ALTER TABLE plan ADD CONSTRAINT FK_DD5A5B7D33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DD5A5B7D33B92F39 ON plan (label_id)');
        $this->addSql('ALTER TABLE project ADD label_id INT NOT NULL, DROP updated_date, DROP created_date, DROP description');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2FB3D0EE33B92F39 ON project (label_id)');
        $this->addSql('ALTER TABLE quotation_status ADD label_id INT NOT NULL, DROP updated_date, DROP created_date, DROP description');
        $this->addSql('ALTER TABLE quotation_status ADD CONSTRAINT FK_8170AF8633B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8170AF8633B92F39 ON quotation_status (label_id)');
        $this->addSql('ALTER TABLE range_module MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE range_module DROP FOREIGN KEY FK_25A2F8582053762E');
        $this->addSql('ALTER TABLE range_module DROP FOREIGN KEY FK_25A2F8587648EE39');
        $this->addSql('DROP INDEX IDX_25A2F8582053762E ON range_module');
        $this->addSql('DROP INDEX IDX_25A2F8587648EE39 ON range_module');
        $this->addSql('ALTER TABLE range_module DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE range_module ADD range_id INT NOT NULL, ADD module_id INT NOT NULL, DROP id, DROP range_id_id, DROP module_id_id');
        $this->addSql('ALTER TABLE range_module ADD CONSTRAINT FK_25A2F8582A82D0B1 FOREIGN KEY (range_id) REFERENCES `range` (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE range_module ADD CONSTRAINT FK_25A2F858AFC2B591 FOREIGN KEY (module_id) REFERENCES module (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_25A2F8582A82D0B1 ON range_module (range_id)');
        $this->addSql('CREATE INDEX IDX_25A2F858AFC2B591 ON range_module (module_id)');
        $this->addSql('ALTER TABLE range_module ADD PRIMARY KEY (range_id, module_id)');
        $this->addSql('ALTER TABLE range_plan MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE range_plan DROP FOREIGN KEY FK_CF3A2A442053762E');
        $this->addSql('ALTER TABLE range_plan DROP FOREIGN KEY FK_CF3A2A442CE2DBAB');
        $this->addSql('DROP INDEX IDX_CF3A2A442053762E ON range_plan');
        $this->addSql('DROP INDEX IDX_CF3A2A442CE2DBAB ON range_plan');
        $this->addSql('ALTER TABLE range_plan DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE range_plan ADD range_id INT NOT NULL, ADD plan_id INT NOT NULL, DROP id, DROP range_id_id, DROP plan_id_id');
        $this->addSql('ALTER TABLE range_plan ADD CONSTRAINT FK_CF3A2A442A82D0B1 FOREIGN KEY (range_id) REFERENCES `range` (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE range_plan ADD CONSTRAINT FK_CF3A2A44E899029B FOREIGN KEY (plan_id) REFERENCES plan (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_CF3A2A442A82D0B1 ON range_plan (range_id)');
        $this->addSql('CREATE INDEX IDX_CF3A2A44E899029B ON range_plan (plan_id)');
        $this->addSql('ALTER TABLE range_plan ADD PRIMARY KEY (range_id, plan_id)');
        $this->addSql('ALTER TABLE range_status ADD label_id INT NOT NULL, DROP updated_date, DROP created_date, DROP description');
        $this->addSql('ALTER TABLE range_status ADD CONSTRAINT FK_5286BB6C33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5286BB6C33B92F39 ON range_status (label_id)');
        $this->addSql('ALTER TABLE storage_component MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE storage_component DROP FOREIGN KEY FK_A1E5BE5812905777');
        $this->addSql('ALTER TABLE storage_component DROP FOREIGN KEY FK_A1E5BE58B0422B79');
        $this->addSql('DROP INDEX IDX_A1E5BE5812905777 ON storage_component');
        $this->addSql('DROP INDEX IDX_A1E5BE58B0422B79 ON storage_component');
        $this->addSql('ALTER TABLE storage_component DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE storage_component ADD storage_id INT NOT NULL, ADD component_id INT NOT NULL, DROP id, DROP storage_id_id, DROP component_id_id');
        $this->addSql('ALTER TABLE storage_component ADD CONSTRAINT FK_A1E5BE585CC5DB90 FOREIGN KEY (storage_id) REFERENCES storage (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE storage_component ADD CONSTRAINT FK_A1E5BE58E2ABAFFF FOREIGN KEY (component_id) REFERENCES component (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_A1E5BE585CC5DB90 ON storage_component (storage_id)');
        $this->addSql('CREATE INDEX IDX_A1E5BE58E2ABAFFF ON storage_component (component_id)');
        $this->addSql('ALTER TABLE storage_component ADD PRIMARY KEY (storage_id, component_id)');
        $this->addSql('ALTER TABLE supplier ADD person_id INT NOT NULL, DROP first_name, DROP last_name, DROP street, DROP city, DROP postal_code, DROP country, DROP email, DROP phone_number, DROP company, DROP libelle');
        $this->addSql('ALTER TABLE supplier ADD CONSTRAINT FK_9B2A6C7E217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_9B2A6C7E217BBB47 ON supplier (person_id)');
        $this->addSql('ALTER TABLE supplier_component MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE supplier_component DROP FOREIGN KEY FK_D3CC9B89A65F9C7D');
        $this->addSql('ALTER TABLE supplier_component DROP FOREIGN KEY FK_D3CC9B89B0422B79');
        $this->addSql('DROP INDEX IDX_D3CC9B89A65F9C7D ON supplier_component');
        $this->addSql('DROP INDEX IDX_D3CC9B89B0422B79 ON supplier_component');
        $this->addSql('ALTER TABLE supplier_component DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE supplier_component ADD supplier_id INT NOT NULL, ADD component_id INT NOT NULL, DROP id, DROP supplier_id_id, DROP component_id_id');
        $this->addSql('ALTER TABLE supplier_component ADD CONSTRAINT FK_D3CC9B892ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplier_component ADD CONSTRAINT FK_D3CC9B89E2ABAFFF FOREIGN KEY (component_id) REFERENCES component (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_D3CC9B892ADD6D8C ON supplier_component (supplier_id)');
        $this->addSql('CREATE INDEX IDX_D3CC9B89E2ABAFFF ON supplier_component (component_id)');
        $this->addSql('ALTER TABLE supplier_component ADD PRIMARY KEY (supplier_id, component_id)');
    }
}
