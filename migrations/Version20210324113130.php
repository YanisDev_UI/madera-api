<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324113130 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE plan DROP FOREIGN KEY FK_DD5A5B7D166D1F9C');
        $this->addSql('DROP INDEX IDX_DD5A5B7D166D1F9C ON plan');
        $this->addSql('ALTER TABLE plan CHANGE project_id quotation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan ADD CONSTRAINT FK_DD5A5B7DB4EA4E60 FOREIGN KEY (quotation_id) REFERENCES quotation (id)');
        $this->addSql('CREATE INDEX IDX_DD5A5B7DB4EA4E60 ON plan (quotation_id)');
        $this->addSql('ALTER TABLE quotation ADD project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE quotation ADD CONSTRAINT FK_474A8DB9166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('CREATE INDEX IDX_474A8DB9166D1F9C ON quotation (project_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE plan DROP FOREIGN KEY FK_DD5A5B7DB4EA4E60');
        $this->addSql('DROP INDEX IDX_DD5A5B7DB4EA4E60 ON plan');
        $this->addSql('ALTER TABLE plan CHANGE quotation_id project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan ADD CONSTRAINT FK_DD5A5B7D166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_DD5A5B7D166D1F9C ON plan (project_id)');
        $this->addSql('ALTER TABLE quotation DROP FOREIGN KEY FK_474A8DB9166D1F9C');
        $this->addSql('DROP INDEX IDX_474A8DB9166D1F9C ON quotation');
        $this->addSql('ALTER TABLE quotation DROP project_id');
    }
}
