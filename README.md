[[_TOC_]]

#### Editions - CONFIGURATION PAR DEFAUT
```yaml
# config/packages/nelmio_cors.yaml
nelmio_cors:
    defaults:
        origin_regex: true
        allow_origin: ['%env(CORS_ALLOW_ORIGIN)%']
        allow_methods: ['GET', 'OPTIONS', 'POST', 'PUT', 'PATCH', 'DELETE']
        allow_headers: ['Content-Type', 'Authorization']
        expose_headers: ['Link']
        max_age: 3600
    paths:
        '^/': null

# config/package/api_platforme
api_platform:
    mapping:
        paths: ['%kernel.project_dir%/src/Entity']
    patch_formats:
        json: ['application/merge-patch+json']
    swagger:
        versions: [3]
    


```

Status:
| System        | Working | Progress |
|---------------|---------|----------|
| Entity        | Yes     | 90%      |
| Repository    | Yes     | 75%      |
| Router        | No      | 0%       |
| JSON Response | No      | 0%       |
| Login         | No      | 0%       |


#### API Madera directory

    editor.md/
            .git/
            bin/
            config/
            Diagram/
            migrations/
            public/
            src/
            var/
            vendor/     
            .env
            composer.json
            composer.lock
            symfony.lock
To configure a user to connect to the database go to the ".env" file and enter the user and login password followed by the default port 3306.
```yaml
###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# IMPORTANT: You MUST configure your server version, either here or in config/packages/doctrine.yaml
# 
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"

 DATABASE_URL="mysql://root:@127.0.0.1:3306/madera-api?serverVersion=5.7"
 
# DATABASE_URL="postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=13&charset=utf8"
###< doctrine/doctrine-bundle ###
```
